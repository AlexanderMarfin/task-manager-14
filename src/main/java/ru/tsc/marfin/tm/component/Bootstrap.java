package ru.tsc.marfin.tm.component;

import ru.tsc.marfin.tm.api.controller.ICommandController;
import ru.tsc.marfin.tm.api.controller.IProjectController;
import ru.tsc.marfin.tm.api.controller.IProjectTaskController;
import ru.tsc.marfin.tm.api.controller.ITaskController;
import ru.tsc.marfin.tm.api.repository.ICommandRepository;
import ru.tsc.marfin.tm.api.repository.IProjectRepository;
import ru.tsc.marfin.tm.api.repository.ITaskRepository;
import ru.tsc.marfin.tm.api.service.ICommandService;
import ru.tsc.marfin.tm.api.service.IProjectService;
import ru.tsc.marfin.tm.api.service.IProjectTaskService;
import ru.tsc.marfin.tm.api.service.ITaskService;
import ru.tsc.marfin.tm.constant.ArgumentConst;
import ru.tsc.marfin.tm.constant.TerminalConst;
import ru.tsc.marfin.tm.controller.CommandController;
import ru.tsc.marfin.tm.controller.ProjectController;
import ru.tsc.marfin.tm.controller.ProjectTaskController;
import ru.tsc.marfin.tm.controller.TaskController;
import ru.tsc.marfin.tm.enumerated.Status;
import ru.tsc.marfin.tm.model.Project;
import ru.tsc.marfin.tm.model.Task;
import ru.tsc.marfin.tm.repository.CommandRepository;
import ru.tsc.marfin.tm.repository.ProjectRepository;
import ru.tsc.marfin.tm.repository.TaskRepository;
import ru.tsc.marfin.tm.service.CommandService;
import ru.tsc.marfin.tm.service.ProjectService;
import ru.tsc.marfin.tm.service.ProjectTaskService;
import ru.tsc.marfin.tm.service.TaskService;
import ru.tsc.marfin.tm.util.DateUtil;
import ru.tsc.marfin.tm.util.TerminalUtil;

import java.util.Date;
import java.util.Scanner;

public final class Bootstrap {

    private final ICommandRepository commandRepository = new CommandRepository();

    private final ICommandService commandService = new CommandService(commandRepository);

    private final ICommandController commandController = new CommandController(commandService);

    private final ITaskRepository taskRepository = new TaskRepository();

    private final ITaskService taskService = new TaskService(taskRepository);

    private final ITaskController taskController = new TaskController(taskService);

    private final IProjectRepository projectRepository = new ProjectRepository();

    private final IProjectService projectService = new ProjectService(projectRepository);

    private final IProjectTaskService projectTaskService = new ProjectTaskService(projectRepository, taskRepository);

    private final IProjectController projectController = new ProjectController(projectService, projectTaskService);

    private final IProjectTaskController projectTaskController = new ProjectTaskController(projectTaskService);

    public void run(final String[] args) {
        if (processArgument(args)) System.exit(0);
        initData();
        commandController.showWelcome();
        final Scanner scanner = new Scanner(System.in);
        while (true) {
            System.out.println("ENTER COMMAND:");
            final String command = TerminalUtil.nextLine();
            processCommand(command);
        }
    }

    public  void initData() {
        taskService.add(new Task("TEST TASK 1", Status.COMPLETED, "Init task", DateUtil.toDate("10.11.2020")));
        taskService.add(new Task("TEST TASK 2", Status.IN_PROGRESS, "Init task", DateUtil.toDate("10.02.2021")));
        taskService.add(new Task("TEST TASK 3", Status.NOT_STARTED, "Init task", DateUtil.toDate("11.08.2021")));
        taskService.add(new Task("TEST TASK 4", Status.NOT_STARTED, "Init task", DateUtil.toDate("12.10.2021")));
        projectService.add(new Project("TEST PROJECT 1", Status.NOT_STARTED, "Init project", DateUtil.toDate("16.03.2018")));
        projectService.add(new Project("TEST PROJECT 2", Status.IN_PROGRESS, "Init project", DateUtil.toDate("23.02.2021")));
        projectService.add(new Project("TEST PROJECT 3", Status.IN_PROGRESS, "Init project", DateUtil.toDate("20.01.2021")));
        projectService.add(new Project("TEST PROJECT 4", Status.COMPLETED, "Init project", DateUtil.toDate("25.11.2020")));
    }

    public void processCommand(final String command) {
        if (command == null || command.isEmpty()) return;
        switch (command) {
            case TerminalConst.VERSION:
                commandController.showVersion();
                break;
            case TerminalConst.ABOUT:
                commandController.showAbout();
                break;
            case TerminalConst.INFO:
                commandController.showSystemInfo();
                break;
            case TerminalConst.ARGUMENT:
                commandController.showArguments();
                break;
            case TerminalConst.COMMAND:
                commandController.showCommands();
                break;
            case TerminalConst.HELP:
                commandController.showHelp();
                break;
            case TerminalConst.TASK_CREATE:
                taskController.createTask();
                break;
            case TerminalConst.TASK_LIST:
                taskController.showTaskList();
                break;
            case TerminalConst.TASK_CLEAR:
                taskController.clearTasks();
                break;
            case TerminalConst.TASK_SHOW_BY_ID:
                taskController.showTaskById();
                break;
            case TerminalConst.TASK_SHOW_BY_INDEX:
                taskController.showTaskByIndex();
                break;
            case TerminalConst.TASK_UPDATE_BY_ID:
                taskController.updateTaskById();
                break;
            case TerminalConst.TASK_UPDATE_BY_INDEX:
                taskController.updateTaskByIndex();
                break;
            case TerminalConst.TASK_REMOVE_BY_ID:
                taskController.removeTaskById();
                break;
            case TerminalConst.TASK_REMOVE_BY_INDEX:
                taskController.removeTaskByIndex();
                break;
            case TerminalConst.TASK_CHANGE_STATUS_BY_INDEX:
                taskController.changeTaskStatusByIndex();
                break;
            case TerminalConst.TASK_CHANGE_STATUS_BY_ID:
                taskController.changeTaskStatusById();
                break;
            case TerminalConst.TASK_START_BY_INDEX:
                taskController.startTaskByIndex();
                break;
            case TerminalConst.TASK_START_BY_ID:
                taskController.startTaskById();
                break;
            case TerminalConst.TASK_COMPLETE_BY_INDEX:
                taskController.completeTaskByIndex();
                break;
            case TerminalConst.TASK_COMPLETE_BY_ID:
                taskController.completeTaskById();
                break;
            case TerminalConst.TASK_BIND_TO_PROJECT:
                projectTaskController.bindTaskToProject();
                break;
            case TerminalConst.TASK_UNBIND_FROM_PROJECT:
                projectTaskController.unbindTaskFromProject();
                break;
            case TerminalConst.TASK_SHOW_BY_PROJECT_ID:
                taskController.showTaskListByProjectId();
                break;
            case TerminalConst.PROJECT_CREATE:
                projectController.createProject();
                break;
            case TerminalConst.PROJECT_LIST:
                projectController.showProjectList();
                break;
            case TerminalConst.PROJECT_CLEAR:
                projectController.clearProjects();
                break;
            case TerminalConst.PROJECT_SHOW_BY_ID:
                projectController.showProjectById();
                break;
            case TerminalConst.PROJECT_SHOW_BY_INDEX:
                projectController.showProjectByIndex();
                break;
            case TerminalConst.PROJECT_UPDATE_BY_ID:
                projectController.updateProjectById();
                break;
            case TerminalConst.PROJECT_UPDATE_BY_INDEX:
                projectController.updateProjectByIndex();
                break;
            case TerminalConst.PROJECT_REMOVE_BY_ID:
                projectController.removeProjectById();
                break;
            case TerminalConst.PROJECT_REMOVE_BY_INDEX:
                projectController.removeProjectByIndex();
                break;
            case TerminalConst.PROJECT_CHANGE_STATUS_BY_INDEX:
                projectController.changeProjectStatusByIndex();
                break;
            case TerminalConst.PROJECT_CHANGE_STATUS_BY_ID:
                projectController.changeProjectStatusById();
                break;
            case TerminalConst.PROJECT_START_BY_INDEX:
                projectController.startProjectByIndex();
                break;
            case TerminalConst.PROJECT_START_BY_ID:
                projectController.startProjectById();
                break;
            case TerminalConst.PROJECT_COMPLETE_BY_INDEX:
                projectController.completeProjectByIndex();
                break;
            case TerminalConst.PROJECT_COMPLETE_BY_ID:
                projectController.completeProjectById();
                break;
            case TerminalConst.EXIT:
                close();
                break;
            default:
                commandController.showErrorCommand(command);
                break;
        }
    }

    public void processArgument(final String arg) {
        if (arg == null || arg.isEmpty()) return;
        switch (arg) {
            case ArgumentConst.VERSION:
                commandController.showVersion();
                break;
            case ArgumentConst.ABOUT:
                commandController.showAbout();
                break;
            case ArgumentConst.INFO:
                commandController.showSystemInfo();
                break;
            case ArgumentConst.ARGUMENT:
                commandController.showArguments();
                break;
            case ArgumentConst.COMMAND:
                commandController.showCommands();
                break;
            case ArgumentConst.HELP:
                commandController.showHelp();
                break;
            default:
                commandController.showErrorArgument(arg);
                break;
        }
    }

    public boolean processArgument(final String[] args) {
        if (args == null || args.length < 1) return false;
        final String arg = args[0];
        processArgument(arg);
        return true;
    }

    public void close() {
        System.exit(0);
    }

}
