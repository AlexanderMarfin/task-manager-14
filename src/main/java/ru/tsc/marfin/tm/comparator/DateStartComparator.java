package ru.tsc.marfin.tm.comparator;

import ru.tsc.marfin.tm.api.model.IHasCreated;
import ru.tsc.marfin.tm.api.model.IHasDateStart;

import java.util.Comparator;

public enum DateStartComparator implements Comparator<IHasDateStart> {

    INSTANCE;
    
    @Override
    public int compare(final IHasDateStart o1, final IHasDateStart o2) {
        if (o1 == null || o2 == null) return 0;
        if (o1.getDateStart() == null || o2.getDateStart() == null) return 0;
        return o1.getDateStart().compareTo(o2.getDateStart());
    }

}
