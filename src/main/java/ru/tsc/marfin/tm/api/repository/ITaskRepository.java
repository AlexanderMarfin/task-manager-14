package ru.tsc.marfin.tm.api.repository;

import ru.tsc.marfin.tm.model.Task;
import ru.tsc.marfin.tm.service.TaskService;

import java.util.Comparator;
import java.util.List;

public interface ITaskRepository {

    Task create(String name);

    List<Task> findAll();

    List<Task> findAll(Comparator comparator);

    List<Task> findAllByProjectId(String projectId);

    Task create(String name, String description);

    Task add(Task task);

    Task findOneByIndex(Integer index);

    Task findOneById(String id);

    boolean existsById(String id);

    Task remove(Task task);

    Task removeByIndex(Integer index);

    Task removeById(String id);

    void clear();

}
