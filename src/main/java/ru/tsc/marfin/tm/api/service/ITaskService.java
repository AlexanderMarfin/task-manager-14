package ru.tsc.marfin.tm.api.service;

import ru.tsc.marfin.tm.enumerated.Sort;
import ru.tsc.marfin.tm.enumerated.Status;
import ru.tsc.marfin.tm.model.Task;

import java.util.Comparator;
import java.util.Date;
import java.util.List;

public interface ITaskService {

    Task create(String name);

    Task create(String name, String description);

    Task create(String name, String description, Date dateStart, Date dateEnd);

    List<Task> findAll();

    List<Task> findAll(Comparator comparator);

    List<Task> findAll(Sort sort);

    List<Task> findAllByProjectId(String projectId);

    Task add(Task task);

    Task findOneByIndex(Integer index);

    Task findOneById(String id);

    Task updateByIndex(Integer index, String name, String description);

    Task updateById(String id, String name, String description);

    Task remove(Task task);

    Task removeByIndex(Integer index);

    Task removeById(String id);

    void clear();

    Task changeTaskStatusByIndex(Integer index, Status status);

    Task changeTaskStatusById(String id, Status status);

}
